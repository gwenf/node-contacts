var fs = require('fs');
var prompt = require('prompt');

function listAllContacts () {
	var contacts = fs.readFileSync('contacts.json');
	console.log(JSON.parse(contacts));
}

// TODO: finish writing this function
function listSingleContact (contactInfo) {
	var contacts = JSON.parse(fs.readFileSync('contacts.json'));

	console.log(contactInfo);
}

function addContact () {
	prompt.start();

	var prompts = [ //should default to null
		'firstname',
		'lastname',
		'email',
		'phone',
		'notes'
	];

	prompt.get(prompts, function (err, result) {
		if (err) throw Error(err);

		var contacts = [];
		try {
			var contactsFile = fs.readFileSync('contacts.json');
			contacts = JSON.parse(contactsFile);
		} catch (e) {
			console.log('Creating contact.json file.');
		}

		contacts.push(result);
		fs.writeFileSync('contacts.json', JSON.stringify(contacts, null, 4));

		console.log('Contact added successfully!');
	});
}

function deleteContact (email) {
	var contacts = fs.readFileSync('contacts.json'),
		deletedContact = null;

	var filteredContacts = contacts.filter(function (contact) {
		return contact.email !== email;
	});

	if (filteredContacts.length !== contacts.length) {
		fs.writeFileSync('contacts.json', filteredContacts);
		console.log('Contact deleted.');
		return filteredContacts;
	} else {
		console.log('No contact with that email found.');
	}
}

module.exports = {
	listAllContacts,
	listSingleContact,
	addContact,
	deleteContact
};
