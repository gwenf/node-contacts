## Contact Table Command Line Application

This command line utility gets contact information via the command line and stores it in a contacts.json file. You can add, delete, search, and list contacts.

### TODO:

* validate inputs
* should be able to update contacts
* should be able to pass flags for additional contact info (custom or something like blog or website)
* should be able to pass a help flag
* make a global utility

