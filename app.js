var yargs = require('yargs');
var argv = yargs.argv;

var utilities = require('./utilities');

(function init () {
	if (argv._.length > 1) {
		console.log('too many arguments');
		return ;
	}

	switch (argv._[0]) {
		case 'help':
			console.log('Help Menu:')
			break;
		case 'add':
			utilities.addContact();
			break;
		case 'list': //default get all contacts, else pass an argument
			console.log(argv.firstname, argv.lastname, argv.email);
			if (!argv.firstname && !argv.lastname && !argv.email) {
				utilities.listAllContacts();
			} else {
				if (argv.email) {
					utilities.listSingleContact({email: argv.email});
				} else if (argv.firstname) {
					utilities.listSingleContact({firstname: argv.firstname});
				} else if (argv.lastname) {
					utilities.listSingleContact({lastname: argv.lastname});
				}

			}
			break;
		case 'delete': //delete by email which must be unique
			if (!argv.email) {
				console.log('Must pass an email to delete a contact.');
				return ;
			}
			utilities.deleteContact(argv.email);
			break;
		default:
			console.log('Invalid Command.');
	}
})()
